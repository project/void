var gulp = require("gulp");
var postcss = require('gulp-postcss');
var prefix = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');

var prefixerOptions = {
  browsers: ['last 2 versions']
};

gulp.task('dist', function () {
  return gulp.src('css/**/*.css')
    .pipe(csscomb())
    .pipe(prefix(prefixerOptions))
    .pipe(gulp.dest('css/'));
});
